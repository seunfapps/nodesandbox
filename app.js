const http = require('http');

const server = http.createServer((req,res) => {
    if(req.url === '/'){
        res.write('Hello my people');
        res.end();
    }
    if(req.url === '/api/names'){
        res.write(JSON.stringify(['Seun','Emmanuel','Fapohunda','Olaribigbe']));
        res.end();
    }
});

server.listen(3000);