function SayHello(name){
    console.log("Hello "+name);
}

SayHello("Seun");

//console.log(module);

//what is returned is an object that has the members you exported from the file
//var logger = require('./logger')// same as require('./logger.js'), node already assumes the file is a js file, so dont put .js at the back again.


var log = require('./logger');
log('Seun is very awesome');

const os = require('os');
//os is a module that gives information about the computer/server.
//this is awesome because regular javascript has access to only the window and dom elements of the browser.

var totalMemory = os.totalmem();
var freeMemory = os.freemem();

//now this is somthing like string interpolation of C#, see the magic
//the character is called back tick
console.log(`Total memory is ${totalMemory} and Free memory is ${freeMemory}`);

//fs is the FileSystem module.
const fs = require('fs');
//methods in node are async by default, there are also sync versions, but always use the default.
fs.readdir('./', function(err, files){
    if(err)console.log('Error', err);
    else console.log('Result', files);
});

//the events module is very core to node
//used to raise events,, duh?!
//peep how the const is in Pascal case, thats because the require call returns a Class.
const EventEmitter = require('events');
//nstantiate object of the class.
var emitter = new EventEmitter();
//register listener for your event
emitter.on('SomethingHappened', function (eventArg){
    console.log(`Yaaay, url = ${eventArg.url}`);
});

//using arrow function
emitter.on('SomethingHappened', (arg) => {
    console.log(`Another yaaay, id is ${arg.id}`);
});

//raise event
emitter.emit('SomethingHappened', {id: 1, url:"http://"});

