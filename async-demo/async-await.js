console.log('before');

async function displayCommits(){
    const user = await getUser(1);
    const repos = await getRepositories(user.gitUserName);
    const commits = await getCommits(repos[0]);
    console.log(commits);
}

displayCommits();

console.log('after');

function getUser(id){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('readding from db');
            resolve({id: id,  gitUserName: 'seunsaber'});
        }, 3000);    
    });

    
    
}
function getRepositories(gitUserName){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`getting repositories for ${gitUserName}`);
            resolve(['repo1', 'repo2', 'repo3']);
        }, 3000);   
    });
  
}

function getCommits(repo){
    return new Promise((resolve, reject) =>{
        setTimeout(() => {
            console.log(`getting commits for ${repo}`);
            resolve(['coomit1', 'commit2', 'commit3']);
        }, 3000);      
    });
    
}