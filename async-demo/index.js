console.log('before');

const user = getUser(1,  (user) => {
    console.log(`User details : ${user.id} name: ${user.gitUserName}`);
    const repos = getRepositories(user.gitUserName, (repos) => {
        console.log(`repos : ${repos}`);
        const commits = getCommits(repos[0], (commits) => {
            console.log(`commits : ${commits}`);
        })
    })
})

console.log('after');

function getUser(id, callback){
    setTimeout(() => {
        console.log('readding from db');
        callback({id: id,  gitUserName: 'seunsaber'});
    }, 3000);
}

function getRepositories(gitUserName, callback){
    setTimeout(() => {
        console.log(`getting repositories for ${gitUserName}`);
        callback(['repo1', 'repo2', 'repo3']);
    }, 3000);   
}

function getCommits(repo, callback){
    setTimeout(() => {
        console.log(`getting commits for ${repo}`);
        callback(['coomit1', 'commit2', 'commit3']);
    }, 3000);      
}