console.log('before');

// const user = getUser(1,  (user) => {
//     console.log(`User details : ${user.id} name: ${user.gitUserName}`);
//     const repos = getRepositories(user.gitUserName, (repos) => {
//         console.log(`repos : ${repos}`);
//         const commits = getCommits(repos[0], (commits) => {
//             console.log(`commits : ${commits}`);
//         })
//     })
// })


const p1 = aCall();
const p2 = anotherCall();
const p3 = yetAnotherCall();
const p4 = failedCall();

Promise.all([p1,p2,p3,p4])
    .then((messages) => console.log(`every one is done: ${messages}`))
    .catch((error) => console.log('One of them failed.', error));

//PROMISE.RACE WOULD RETURN AS SOON AS ONE OF THE PROMISES IS DONE.
//Promise.race([p1,p2,p3])
    //.then((messages) => console.log(`every one is done: ${messages}`));

console.log('after');

function aCall(){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Call 1');
            resolve('yaay, done.');
        }, 4000);    
    });
    
}
function anotherCall(){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('Call 2');
            resolve('nice, I am done now');
        }, 3000);   
    });
  
}

function yetAnotherCall(){
    return new Promise((resolve, reject) =>{
        setTimeout(() => {
            console.log('Call 3');
            resolve('I am so done');
        }, 3500);      
    });
    
}

function failedCall(){
    return new Promise((resolve, reject) =>{
        setTimeout(() => {
            console.log('Call 4');
            reject(new Error('someting wrong here'));
        }, 3200);      
    });
    
}