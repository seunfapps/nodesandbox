console.log('before');

// const user = getUser(1,  (user) => {
//     console.log(`User details : ${user.id} name: ${user.gitUserName}`);
//     const repos = getRepositories(user.gitUserName, (repos) => {
//         console.log(`repos : ${repos}`);
//         const commits = getCommits(repos[0], (commits) => {
//             console.log(`commits : ${commits}`);
//         })
//     })
// })


getUser(1)
    .then(user => getRepositories(user.gitUserName))
    .then(repos =>  getCommits(repos[0]))
    .then(commits => console.log(`commits ${commits}`))
    .catch(err => console.log(`Error ${err}`));

console.log('after');

function getUser(id){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('readding from db');
            resolve({id: id,  gitUserName: 'seunsaber'});
        }, 3000);    
    });
    
}
function getRepositories(gitUserName){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log(`getting repositories for ${gitUserName}`);
            resolve(['repo1', 'repo2', 'repo3']);
        }, 3000);   
    });
  
}

function getCommits(repo){
    return new Promise((resolve, reject) =>{
        setTimeout(() => {
            console.log(`getting commits for ${repo}`);
            resolve(['coomit1', 'commit2', 'commit3']);
        }, 3000);      
    });
    
}