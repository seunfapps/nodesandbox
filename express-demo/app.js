const express = require('express');
const config = require('config');//config helps us with configuration
const helmet = require('helmet'); // for http headers
const morgan = require('morgan'); // for logging http activity
const appDebugger = require('debug')('app:startUp');

const courses = require('./routes/courses');
const home = require('./routes/home');

const logger = require('./middleware/logger');
const auth = require('./auth');
const app = express();

app.set('view engine', 'pug');
app.set('views', './views');

app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(express.static('public'));
app.use(helmet());

app.use('/', home);
app.use('/api/courses', courses);//when you see a route that starts with '/api/courses, use the courses router.

console.log(`Application Name ${config.get('name')}`);
console.log(`Mail Server ${config.get('mail.host')}`);


//console.log(`Mail Password ${config.get('mail.password')}`);
if(app.get('env') === 'development'){
    app.use(morgan('dev'));
    //console.log('Morgan is enabled');
    appDebugger('Morgan is enabled');
}

app.use(logger);
app.use(auth);



const port = process.env.PORT || 3000;
app.listen(port, () => { console.log(`Listening on port ${port}`)});