const express = require('express');
const Joi = require('joi'); //Joi is a package that helps with validation
const config = require('config');//config helps us with configuration
const helmet = require('helmet'); // for http headers
const morgan = require('morgan'); // for logging http activity
const appDebugger = require('debug')('app:startUp')//you can call it anything you like , so you could identify the type of debug it is, //startUp may be because of errors that could happen during the startUp part of your code
const dbDebugger = require('debug')('app:db');
const httpDebugger = require('debug')('app:http');
/**
 * TO use the debug package in code, set the DEBUG env variable to the namespaces e.g 'app:startUp' that you want to see their logs
 * cmd => set DEBUG=app:startUp, this will only show logs of that namespace.
 * to show from all namespaces cmd => set DEBUG=app:*
 * to reset the env variable cmd => set DEBUG=
 */

const logger = require('./middleware/logger');
const auth = require('./auth');
const app = express();
//adds a middleware that allows us read post body as json.
app.use(express.json());
//for when body is passed as form url encodeed something like fname=Seun&lname=Willy
app.use(express.urlencoded({ extended: true}));
//to serve static files, pass in name of folder where your static files are.
app.use(express.static('public'));
app.use(helmet());


app.set('view engine', 'pug');//here we are setting the view engine we want to use, we are using pug
app.set('views', './views');


//configurations
//the config object can scan different sources for values
console.log(`Application Name ${config.get('name')}`);
console.log(`Mail Server ${config.get('mail.host')}`);

//this 'console.log(`Mail Password ${config.get('mail.password')}`);' is gotten from custom-environment-variables.json file
//the value of the mail.password property :'express_app_pwd' is a environment variable
//which stores the actual password.
//the names of the json files in the config folder must be accurate, no typos.
console.log(`Mail Password ${config.get('mail.password')}`);
if(app.get('env') === 'development'){
    app.use(morgan('dev'));
    //console.log('Morgan is enabled');
    appDebugger('Morgan is enabled');
}



//custom middleware
app.use(logger);
app.use(auth);

//some db work
dbDebugger('Connected to Database!');

const courses = [
    {
        "id": 1,
        "name": "Physics"
    },
    {
        "id": 2,
        "name": "Chemistry"
    },
    {
        "id": 3,
        "name": "Math"
    },
    {
        "id": 4,
        "name": "English"
    },
];
app.get('/', (req, res) => {
    //res.send('Hello Express!!!');
    //returning a view template
    httpDebugger('returning view');
    res.render('index', {title:'Seun rocks', message: 'YOu know it.'});
});

app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) => {

    const course = courses.find(c => c.id === parseInt(req.params.id));

    if(!course) res.status(404).send(`A course with id ${req.params.id} was not found,`);

    res.send(course);
});
//use route parameters for essential stuff, use query param for optional stuff  
app.get('/api/posts/:year/:month/:day', (req, res) => {
    res.send(req.params);

    //IF THERE ARE QUERY PARAMS IN THE GET CALL
    //IT WILL BE STORED IN THE req.query OBJECT
    //KEY-VALUE STYLES.
    //res.send(req.query);
});

app.post('/api/courses', (req, res) => {    
    //this is object destructuring, where you can pick a property of an object

    const {error} = validateCourse(req.body)

    if(error){
        //400 bad request
        //if you console.log validationResult, you'll see that the 
        //actual error message is in the error object, 
        //an array of details, pick the first,
        //or you may want to iterate over the array and concatenate
        res.status(400).send(error.details[0].message);
        return;
    }

    const course = {
        id : courses.length + 1,
        name : req.body.name
    };
    courses.push(course);
    res.send(course);
});

app.put('/api/courses/:id', (req,res) => {
    //find
    let course = courses.find(c => c.id === parseInt(req.params.id));
    console.log(course);
    if(!course) return res.status(404).send(`A course with id ${req.params.id} was not found!`);    

    //validate
    const {error} = validateCourse(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //update
    course.name = req.body.name;
    res.send(course);

});

app.delete('/api/courses/:id', (req,res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send(`A course with the id ${req.param.id} not found`);

    courses.splice(courses.indexOf(course), 1);

    res.send(course);
});
function validateCourse(course){
    //using JOI
    //JOi schema
    const schema = {
        name : Joi.string().min(3).required()
    };
    
    //validate request against schema
    return Joi.validate(course, schema);    
}
//envirommental variables (set outside applictation)
//the process object gives access to thr env variable
//PORT is the name of the environmental variable
//you can set environmental variables in cmd
//set PORT=5000
const port = process.env.PORT || 3000;
app.listen(port, () => { console.log(`Listening on port ${port}`)});