const express = require('express');
const Joi = require('joi'); //Joi is a package that helps with validation
//const app = express(); this would not work since we moved the routes to this module.
const router = express.Router();

const courses = [
    {
        "id": 1,
        "name": "Physics"
    },
    {
        "id": 2,
        "name": "Chemistry"
    },
    {
        "id": 3,
        "name": "Math"
    },
    {
        "id": 4,
        "name": "English"
    },
];


router.get('/', (req, res) => {
    res.send(courses);
});

router.get('/:id', (req, res) => {

    const course = courses.find(c => c.id === parseInt(req.params.id));

    if(!course) res.status(404).send(`A course with id ${req.params.id} was not found,`);

    res.send(course);
});

router.post('/', (req, res) => {    
    const {error} = validateCourse(req.body)
    if(error) return res.status(400).send(error.details[0].message);

    const course = {
        id : courses.length + 1,
        name : req.body.name
    };
    courses.push(course);
    res.send(course);
});

router.put('/:id', (req,res) => {
    //find
    let course = courses.find(c => c.id === parseInt(req.params.id));
    console.log(course);
    if(!course) return res.status(404).send(`A course with id ${req.params.id} was not found!`);    

    //validate
    const {error} = validateCourse(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    //update
    course.name = req.body.name;
    res.send(course);

});

router.delete('/:id', (req,res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send(`A course with the id ${req.param.id} not found`);

    courses.splice(courses.indexOf(course), 1);

    res.send(course);
});

function validateCourse(course){
    
    const schema = {
        name : Joi.string().min(3).required()
    };
    
    //validate request against schema
    return Joi.validate(course, schema);    
}

module.exports = router;