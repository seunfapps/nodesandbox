const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', {title:'Seun rocks', message: 'YOu know it.'});
});

module.exports = router;