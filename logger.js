const EventEmitter = require('events');
class Logger extends EventEmitter{
    log(message){
        console.log(`Yo, I got your message, ${message}`);
        
        //raise event
        this.emit('SomethingLogged', {id : 1});
    }   
}

module.exports = Logger;