const logger = require('./logger');

module.exports = function(err, req, res, next){
    //this only handles exception from express route, remember we installed 'express-async-errors'
    //log it somewhere
    console.log('Oops, something went wrong here ', err);
    logger.error('A route exception occureed', err);
    res.status(500).send('Something went horribly wrong');
    next();
}