const mongoose = require('mongoose');
const Joi = require('joi');

const customerSchema = mongoose.Schema({
    isGold: Boolean,
    name: {
        type: String,
        required: true        
    },
    phone : String
});

const Customer = mongoose.model('customer', customerSchema);

function validate(customer){
    const schema = {
        name: Joi.string().min(3).required(),
        isGold: Joi.boolean().required(),
        phone: Joi.string().min(7).required()
    };

    return Joi.validate(customer, schema);
}


module.exports.Customer = Customer;
module.exports.validate = validate;
module.exports.customerSchema = customerSchema;