const Joi = require('joi');
const mongoose = require('mongoose');

const genreSchema = mongoose.Schema({
    name: {
        type: String,
        require: true,
        minlength: 3
    }
});

const Genre = mongoose.model('genre', genreSchema);

function validate(genre){
    const schema = {
        name : Joi.string().min(3).required()
    };

    return Joi.validate(genre,schema);
}

module.exports.Genre = Genre;
module.exports.validate = validate;
module.exports.genreSchema = genreSchema;
