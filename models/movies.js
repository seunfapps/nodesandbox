const mongoose = require('mongoose');
const Joi = require('joi');
const {genreSchema} = require('./genres'); //we have already created a genreSchema in the genre model
//just export it and use here

const movieSchema = new mongoose.Schema({
    title: String,
    genre: {
        type: genreSchema,
        required: true
    },
    numberInStock: {
        type: Number,
        default: 0
    },
    dailyRentalRate: {
        type: Number,
        default: 0
    }
});
const Movie = mongoose.model('movie', movieSchema);


function validate(movie){
    const schema = {
        title: Joi.string().required(),
        genreId : Joi.objectId().required()
    };

    return Joi.validate(movie, schema);
}


module.exports.validate = validate;
module.exports.Movie = Movie;
module.exports.movieSchema = movieSchema;