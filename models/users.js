const jwt = require('jsonwebtoken');
const config = require('config');
const mongoose = require('mongoose');
const Joi = require('joi');

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        minlength: 2
    },
    email:{
        type: String,
        required: true,
        unique: true
    },
    password:{
        type: String,
        required: true,
        minlength: 6
    },
    isAdmin: Boolean
});

userSchema.methods.generateToken = function () {
    return jwt.sign({_id: this._id, name: this.name, isAdmin: this.isAdmin}, config.get('jwtPrivateKey'));
}

const User = mongoose.model('User', userSchema);


function validate(user){
    const schema =  {
        name: Joi.string().required().min(2),
        email: Joi.string().required().email(),
        password: Joi.string().required().min(6)
    };

    return Joi.validate(user, schema);
}

module.exports.User = User;
module.exports.validate = validate;