const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/mongo-exercises')
    .then(() => console.log('connected to db'))
    .catch(err => console.log('error occurred ', err));


const courseSchema = new mongoose.Schema({
    tags:[String],
    date: {type:Date, default:Date.now},
    name: String,
    author: String,
    isPublished: Boolean,
    price: Number
});

const Course = mongoose.model('Course', courseSchema);

async function getCourses(){
    return await Course.find({isPublished: true, tags:'backend'})
    .sort({name : 1})
    .select({name: 1, author: 1});
}

async function getCourses2(){
    return await Course.find({isPublished: true, tags:{ $in: ['frontend', 'backend']}})
    //.or([{tags : 'frontend'}, {tags: 'backend'}])
    .sort({price : -1})
    .select({name: 1, author: 1, price : 1});
}

async function getCourses3(){
    return await Course.find({isPublished: true})
    .or([{price : {$gte : 15}}, {name: /.*by.*/i}])
    .sort({price : -1})
    .select({name: 1, author: 1, price : 1});
}

async function run(){
    //const courses = await getCourses();
    //const courses = await getCourses2();
    const courses = await getCourses3();
    console.log('the courses are : ', courses);
}
run();