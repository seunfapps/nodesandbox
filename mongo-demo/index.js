const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/playground')
    .then(() => console.log('Successfully connected to mongodb'))
    .catch(err => console.log('Could not connect to MongoDB ', err));

/**A collection in MongoDB is like a table,
 * A document is kind of similar to a row
 * 
 * A schema is like a model, that the document (row) conforms to */

const courseSchema = new mongoose.Schema({
    name: { 
        type:String, 
        required: true,
        minlength: 3,
        maxlength: 200
    },
    author: String,
    tags: {
        type: Array,
        validate:{
            isAsync: true,
            validator: function (tags, callback){
                //do some async work
                setTimeout(() =>{
                    callback(tags.length > 0)//resolves to true ot false
                }, 3000);
                //return tags.length > 0;
            },
            message: 'A course must have at least one tag'
        }
    },
    date: {type: Date, default: Date.now},
    isPublished: Boolean,
    price: {
        type: Number,
        required: function() {return this.isPublished;},//only required if this is published,
        min: 2,
        max: 2000,
        //get and set are functions, 
        get: v => Math.round(v), //when reading price from DB, round the value
        set: v => Math.round(v)// when setting price, round value. 
    },
    category:{
        type: String,
        required: true,
        enum: ['web', 'mobile', 'dev-ops'] //this is saying, these are the only acceptable values
    }
});

/**
 * Permitted schema types in mongoose schema
 * String, Number, Date, Buffer(for binary data), Boolean, ObjectID(unique identiifer), Array
 */

//creating a model
//mongoose.model returns a Class, that you create instances of to make objects(models)
//the first parameter is the name the collection would be called, use singular name
//the second parameter is the schema

const Course = mongoose.model('Course', courseSchema);

async function createCourse(){
    //const Course = mongoose.model('Course', courseSchema);
    //instantiaring a new object of the class
    const course = new Course({
        name: 'F',
        author: 'Pablo Van Gogh',
        tags: [],
        isPublished: true,
        category: 'my cat'
    });
    
    //actually save the object to mongodb
    try{
        const result = await course.save();
        console.log(result);
    }
    catch(ex){
        //this would log all the error messages as one string, comma seperated.
        console.log(ex.message);
        console.log();//line break
        //if you want to get a particular error,
        //the exception object, has an 'errors' object, that contains 
        //objects relating to each validation rule
        //Example
        console.log(`Hey, Error found on Name: ${ex.errors.name.message}`);
        console.log(`Hey, Error found on tags: ${ex.errors.tags.message}`);
        console.log();
        //you could also iterate over the errors object 
        for(field in ex.errors){
            console.log(`Looping, found error: ${ex.errors[field].message}`)
        }

    }
    
}

async function getCourses(){
    //const courses = await Course.find();
    /**
     * IN MONGO DB, COMPARISON OPERATORS ARE TOTALLY DIFFERENT
     * eq equal
     * ne not equal
     * gt greater than
     * gte greater than or equal to
     * lt less than
     * in
     * nin not in
     */

     /**LOGICAL OPERATORS
      * and
      * or
      */


    const pageNumber = 2;
    const pageSize =  10;

    //should be passed as route queries : /api/courses?pageNumber=2&pageSize=10

    const courses = await Course
        .find({author: 'Albert Newton', isPublished: true})        
        .skip(pageNumber - 1) * pageSize
        .limit(pageSize)
        .sort({name: 1})//sort by name 1 ascending -1 descending
        .count();//return count
    ;

    console.log(courses);
}

async function updateCourse(id){
   //Course.update returns the result of the operation, not a course object
   //Course.findByIdAndUpdate returns the OLD course object
   //passing a parameter object {new : true } to the findByIdAndUpdate lets it return the NEWly updated course
   const result = await Course.findByIdAndUpdate({_id:id},{
        $set : {
            //this is a mongob update operator : $set, there are quite a few of them. check them out
             author: "Wale Adenuga",
             isPublished: false
        }
   }, {new : true});

   console.log(result);
}

async function removeCourse(id){
    const result = await Course.deleteOne({_id: id});
    //to get the deleted item
    //const course = await Course.findByIdAndRemove({_id:id});
    console.log(result);
 }
createCourse();
//getCourses();
//updateCourse('5ba6d69a79a6d34ce8b11468');
//removeCourse('5ba6d69a79a6d34ce8b11468');