const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground-embedding')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const authorSchema = new mongoose.Schema({
  name: String,
  bio: String,
  website: String
});

const Author = mongoose.model('Author', authorSchema);

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  authors: [authorSchema] //this is embedding, kinda like C# property, get it?
  /**
   * public class Course
   * {
   *    public string Name {get; set;}
   *    public List<Author> Author {get; set;}
   * }
   */
}));

async function createCourse(name, authors) {
  const course = new Course({
    name, 
    authors
  }); 
  
  const result = await course.save();
  console.log(result);
}


async function updateAuthor(courseId){
  //find the course
  //let course = await Course.findById(courseId);
  //modify
  //course.author.name = 'Seun Fapps';
  //update
  //course.save();

  /**Remember the other way to update. The update directly at the DB, one call. */
  const course = await Course.update({_id: courseId}, {
    $set: {
        'author.name': 'Seun Faphunda'
    }
  });
}

async function addAuthor(courseId, author){
  const course = await Course.findById(courseId);
  course.authors.push(author);
  course.save();
}

async function removeAuthor(courseId, authorId){
  const course = await Course.findById(courseId);
  const author = course.authors.id(authorId);
  author.remove();

  course.save();
}
async function listCourses() { 
  const courses = await Course.find();
  console.log(courses);
}

// createCourse('Another Course', 
//   [ 
//     new Author({ name: 'Seun Kesa' }),
//     new Author({ name: 'Emmanuel Fernandez' }),
//     new Author({ name: 'Olaribigbe Wellington' })
//   ]);


//updateAuthor('5bb7f7f120f9262ec8e50829');
//addAuthor('5bb7fd7eb8bc7209e87dffb3', new Author({name: 'Adeyera Atanda'}));
removeAuthor('5bb7fd7eb8bc7209e87dffb3', '5bb7fd7eb8bc7209e87dffb1');
