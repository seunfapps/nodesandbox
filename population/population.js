const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground-population')
  .then(() => console.log('Connected to MongoDB...'))
  .catch(err => console.error('Could not connect to MongoDB...', err));

const Author = mongoose.model('Author', new mongoose.Schema({
  name: String,
  bio: String,
  website: String
}));

const Course = mongoose.model('Course', new mongoose.Schema({
  name: String,
  author: {
      type: mongoose.Schema.Types.ObjectId, //id of the author document you want to reference
      ref: 'Author' //the name of the collection(table) you are referencing 
  }
}));

async function createAuthor(name, bio, website) { 
  const author = new Author({
    name, 
    bio, 
    website 
  });

  const result = await author.save();
  console.log(result);
}

async function createCourse(name, author) {
  const course = new Course({
    name, 
    author
  }); 
  
  const result = await course.save();
  console.log(result);
}

async function listCourses() { 
  const courses = await Course
    .find()
    //.populate('author')//kinda like a join, retreiving the data stored in the author document where applicable
    .populate('author', 'name')//selecting a particular column
    .select('name');
  console.log(courses);
}

//createAuthor('Mosh', 'My bio', 'My Website');

// createCourse('Node Course', '5bb7ea1a82d7f04934893b12')

 listCourses();