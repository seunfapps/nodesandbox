const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const validateObjectId = require('../middleware/validateObjectId');
const express = require('express');
const {Genre, validate} = require('../models/genres');
const mongoose = require('mongoose');
const router = express.Router();
// const asyncMiddleware = require('../middleware/async');


//get all genres
router.get('/', async (req,res) => {    
    const genres = await Genre.find().sort('name');
    res.send(genres);
    console.log('successful - fetch all');
});

router.get('/exception', async(req,res, next) =>{
    //testing unhandled exception
    try{
        throw 'An Evil Exception';
        //throw new Error('An Evil Exception');
    }catch(ex){
        //THE NEXT MIDDLEWARE FUNCTION TO EXECUTE IS THE EXCEPTION HANDLER MIDDLEWARE FUNCTION I CREATED
        //SO THERE IS A SINGLE POINT TO CHANGE HOW ALL ERRORS ARE HANDLED
        //JUST CALL next(ex) AND PASS THE EXCEPTION OBJECT AS AN ARGUMENT
        next(ex);
    }
});

router.get('/rawexception', async(req,res, next ) =>{
   throw 'An uncaught exception';
});
//get one genre
/**Now, listen carefully...
 * instead of putting try-catch block, with next() in every route.
 * we can move the try catch thing to a middleware
 * we can now pass the route handler function i.e (req,res) => {} as an arguement, so that we can wrap a try catch around it.
 * so below we are calling => asyncMiddleware((req, res) => {}) by passing the function as a parameter
 */
router.get('/:id', validateObjectId, async(req,res) => {
    
    const genre = await Genre.findById(req.params.id);
    if(!genre) res.status(404).send(`The genre with id ${req.params.id} was not found.` );
    //throw 'Oga stop here';
    res.send(genre);
    console.log('successful - fetch one');
});

//create genre
router.post('/', auth, async (req, res) => {
    //to use the auth middlewre, the 2nd parameter is optional middleware object.
    //its kinda like decorating your actions with attributes in C# e.g [Authorize]


    //Joi validation
    const {error} = validate(req.body);
    if(error)return res.status(400).send(error.details[0].message);

    let genre = new Genre({
        name: req.body.name
    });

    genre = await genre.save();

    res.send(genre);

    console.log('successful - add');
});

//update genre
router.put('/:id', async (req,res) => {
    console.log('Put request came in');

    const {error} = validate(req.body);
    if(error)return res.status(400).send(error.details[0].message);

    console.log('Valid, about to make call');

    const genre = await Genre.findByIdAndUpdate(req.params.id, { name : req.body.name},{ new: true});

    res.send(genre);

    console.log('successful - update');

});

/**IMPLEMENTING ONLY ADMIN CAN DELETE
 * 
 * TO USE MULTIPLE MIDDLEWARE, PUT THEM IN AN  ARRAY
 * THE WOULD BE EXECUTED ONE AFTER THE OTHER, i.e next()
 */
//delete genre
router.delete('/:id', [auth, admin], async (req,res) => {
    console.log('deleting');

    if(!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send('Invalid ID');

    const genre = await Genre.findByIdAndDelete(req.params.id);
    res.send(genre);

    console.log('successful - delete one');
});

module.exports = router;