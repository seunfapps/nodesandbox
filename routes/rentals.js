const express = require('express');
const router = express.Router();
const {Rental, validate} = require('../models/rentals');
const {Customer} = require('../models/customers');
const {Movie} = require('../models/movies');
const mongoose = require('mongoose');
const Fawn = require('fawn');

Fawn.init(mongoose);

router.get('/', async (req, res) => {
    const rentals = await Rental.find();
    res.send(rentals);
});

router.post('/', async (req, res) => {

    const {error} = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);

    const customer = await Customer.findById(req.body.customerId)
        .catch(err => res.status(400).send('oops, Invalid Customer ID'));
    if(!customer)
        return res.status(400).send('Invalid Customer');

    const movie = await Movie.findById(req.body.customerId)
    .catch(err => res.status(400).send('oops, Invalid MOvie ID'));
    if(!movie)
        return res.status(400).send('Invalid Movie');

    let rental = new Rental({
        customer: {
           _id: customer._id, 
           isGold: customer.isGold,
           name: customer.name,
           phone: customer.phone
        },
        movie: {
            _id: movie._id,
            title: movie.title,
            dailyRentalRate: movie.dailyRentalRate
        }
    });


    /**Fawn is used to implement Atomicity (Transactions) for Mongo, 
     * If one dies, we all die!.
     */
    try {    
        new Fawn.Task()
        //EVERYTHING YOU CHAIN HERE WOULD BE CONSIDERED ONE TRANSACTION, ATOMICITY
        .save('rentals', rental)//FIRST PARAMETER IS THE NAME OF THE COLLECTION, ITS CASE SENSSITIVE!, 2ND PARAM IS THE OBJECT
        .update('movies', {_id: movie._id}, {
            $inc: {
                //$inc is an operator that increments a property by a value
                numberInStock : -1 // this is equivalent to numberInStock--;
            }
        } )//1ST PARAM IS THE COLLECTION, 2ND PARAM IS THE QUERY OBJECT, 3RD PARAM IS THE ACTUAL UPDATE OBJECT
        .run()//AFTER YOU ARE DONE CHAINING YOUR STUFF, CALL THE RUN METHOD TO ACTUALLY RUN THIS AGAINST THE DB
      
        res.send(rental);
    }
    catch(ex)
    {
        res.status(500).send('Something went wrong');
    }
    
});


module.exports = router;