const {Rental} = require('../models/rentals');
const {Movie} = require('../models/movies');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const moment = require('moment');


router.post('/', auth, async (req, res) => {
    console.log(req.body);
    if(!req.body.customerId || !req.body.movieId)
        return res.status(400).send('Bad request');

    const rental = await Rental.lookup(req.body.customerId, req.body.movieId);

    if(!rental)
        return res.status(404).send('Rental object not found');

    if(rental.dateReturned)
        return res.status(400).send('Rental already processed');


    rental.dateReturned = new Date();

    const dateRented = moment(rental.dateRented);
    const dateReturned = moment(rental.dateReturned);

    const numberOfDays = dateReturned.diff(dateRented, 'days');
    
    rental.price = numberOfDays * rental.movie.dailyRentalRate;

    await rental.save();

    await Movie.update({ _id: rental.movie._id}, {
        $inc : {
            numberInStock: 1
        }
    });
    

    res.send(rental);

   
});

module.exports = router;