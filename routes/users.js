const express = require('express');
const auth = require('../middleware/auth');
const {User, validate} = require('../models/users');
const router = express.Router();
const _ = require('lodash');
const bcrypt = require('bcryptjs');

router.get('/', async(req, res) => {
    const users = await User.find();
    res.send(users);
});

//the positioning of this route matters, if you put it below /:id
//it'll assume 'me' as an id you are passing as a param.
router.get('/me', auth, async(req, res) => {
    //if the auth middleware fails, it won't bother running this code.

    //res.send(req.user);
    const user = await User.findById(req.user._id);
    
    const _user = _.pick(user, ['_id', 'name', 'email']);

    /**
     * you can also do this, to remove the password field
     * const _user = await User.findById(req.user._id).select('-password')
     */
    res.send(_user);
});


router.get('/:id', async(req, res) => {
    const user = await User.findById(req.params.id).select('-password');
    if(!user)
        return res.status(400).send('User not found');
   
    res.send(user);
});

router.post('/', async(req,res)=>{
    const {error} = validate(req.body);
    if(error)
        return res.status(400).send(`Invalid request  ${error.details[0].message}`);

    let user = await User.findOne({email : req.body.email});
    if(user)
        return res.status(400).send('User already exists');

    // user = new User({
    //     name: req.body.name,
    //     email: req.body.email,
    //     password: req.body.password
    // });

    
    //this is same with what is commented above
    user = new User(_.pick(req.body, ['name', 'email', 'password']));
    

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();

    //lodash is basically an optimized underscore
    //the pick function takes properties of an object (as an array of strings) and returns a new object
    //with only those you picked
    //reason behind this is, we don't want to return password back in the objects
    

    //after registering the user, we may want to generate a token, so the user may proceed doing other stuff, 
    //instead of forcing them to log in again before we can get token

    //const token = jwt.sign(user, config.get('jwtPrivateKey'));
    const token = user.generateToken();
    //res.send(user);

    user = _.pick(user, ['name', 'email']);

    //now we are storing the token in the header, naming convention is x-{my-variable}
    res.header('x-auth-token', token).send(user);

});
router.put('/', async(req, res)=>{});
router.delete('/:id', async(req,res) => {});

module.exports = router;