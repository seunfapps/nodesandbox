const logger = require('../middleware/logger');
require('express-async-errors');
module.exports = function(){
    //the process object emits an event 'uncaughtException'
    //the event occurs when an exception is raised outside of a try-catch block
   
    process.on('uncaughtException', e => {
        //this would be raised when exception occurs in synchronous code
       
        logger.error(`This is  really bad bro : ${e.message}. Stack: ${e.stack}` , e);

        //AS BEST PRACTICE, YOU SHOULD TERMINATE WHEN AN UNCAUGHT EXCEPTION OCCURS
        //process.exit(1); //0 is success, every other thing is failed

        //process manager would help you restart in prod
    });


    //TO CATCH UNHANDLED PROMISE REJECTIONS
    //i.e when you don't use catch on your promises, or you forget to use auait.
    process.on('unhandledRejection', e =>{
        logger.error(e.message, e);

       // process.exit(1); //0 is success, every other thing is failed
    });

}