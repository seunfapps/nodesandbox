const exercise = require('../exercise1');

describe('FizzBuzz', () => {
    it('should throw if input is not number', () => {
        expect(() => exercise.fizzBuzz('sfsf')).toThrow();
    });

    it('should return FizzBuzz if input is divisible by 3 and 5', () => {
        const result = exercise.fizzBuzz(15);
        expect(result).toBe('FizzBuzz');
    });

    it('should return Fizz if input is divisible by 3', () => {
        const result = exercise.fizzBuzz(9);
        expect(result).toBe('Fizz');
    });

    it('should return Buzz if input is divisible by 5', () => {
        const result = exercise.fizzBuzz(5);
        expect(result).toBe('Buzz');
    });

    it('should return input if input is not divisible by 3 or 5', () => {
        const result = exercise.fizzBuzz(7);
        expect(result).toBe(7);
    });
});