const lib = require('../lib');
const db = require('../db');
const mail = require('../mail');

//BASICALLY ITS THE SAME AS THE UNIT TEST YOU KNOW
//JUST A LITTLE CHANGE IN SYNTAX
//CHECK JEST DOCUMNETATIO FOR MORE

//grouping related tests
describe('absolute', () => {
    //IN JEST, WE CAN REPLACE test WITH it
    //NOTICE HOW READABLE IT IS NOW
    it('should return positive if input is positive', () => {
        //arrange
        const number = 1;
        //act
        const response = lib.absolute(number);
        //assert
        expect(response).toBe(1);
    });
    
    it('should return positve if inpit is negative', () => {
        //arrange
        const number = -1;
        //act
        const response = lib.absolute(number);
        //assert
        expect(response).toBe(1);
    });
    
    it('should return 0 if input is 0', () => {
        //arrange
        const number = 0;
        //act
        const response = lib.absolute(number);
        //assert
        expect(response).toBe(0);
    });
});

describe('greet', () => {
    it('should return the greeting message with name', () => {
        const result = lib.greet('Seun');
        expect(result).toContain('Seun');
    })
});

describe('getCurrencies', () => {
    it('should return supported currencies', () => {
        const result = lib.getCurrencies();

        expect(result).toEqual(expect.arrayContaining(['USD', 'AUD', 'EUR']));
    })
});

describe('getProduct', () => {
    it('should return product with given id', () =>{
        const result = lib.getProduct(1);

        expect(result).toEqual({id: 1,  price: 10});
        expect(result).toHaveProperty('id', 1);
    })
});

describe('registerUser', () => {

    it('should throw if username is falsy', () => {
        /**
         * Falsy values in JS
         * Null, undefined, NaN, '', 0, false
         */

         const args = [null, undefined, NaN, '', 0, false];

         args.forEach(a => {
             expect(() => { lib.registerUser(a)}).toThrow();
         })
    });

    it('should return a user object if valid username is passed', () => {
        const result = lib.registerUser('seunfapps');

        expect(result.username).toContain('seunfapps');
        expect(result.id).toBeGreaterThan(0);

    }); 
});

describe('applyDiscount', () => {
    it('should apply a 10% discount is customer points > 10', () => {
        //since this lib.applyDiscount function talks to an external resource, we should mock it.
        //IN JS, MOCKING IS REALLY SIMPLE, JUST CHANGE THE IMPLEMENTATION OF THE FUNCTION!.

        db.getCustomerSync = function(customerId){
            //fake going to db to fetch           
            return {id: customerId, points: 11};
        }

        const order = { customerId: 1, totalPrice: 100};
        const result = lib.applyDiscount(order);

        expect(order.totalPrice).toBe(90);
    })
});

describe('notifyCustomer', () => {
    it('should send an email to the customer',  () => {
        // db.getCustomerSync = function(customerId){
        //     //fake going to db to fetch           
        //     return {email: 's'};
        // }

        //jest.fn() returns an empty function
        //you can now chain what you want to it

        //we can make it return a promise too, cool : mockResolvedValue({email: s}), mockRejectedValue(new Error('wahala dey))
        db.getCustomerSync = jest.fn().mockReturnValue({email : 's'});
        mail.send = jest.fn();
        
        // let mailSent = false;

        // mail.send = function(to, subject){
        //     mailSent = true;
        // }

        lib.notifyCustomer({customerId : 1});

        //expect(mailSent).toBe(true);
        expect(mail.send).toHaveBeenCalled();// this is only available on jest.fn();
    });
})

