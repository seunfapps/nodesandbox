const request = require('supertest');
const {Rental} =  require('../../../models/rentals');
const {User} =  require('../../../models/users');
const {Movie} = require('../../../models/movies');
const mongoose = require('mongoose');
const moment = require('moment');
describe('/api/returns', () => {
    
    let server;
    let customerId;
    let movieId;
    let token;
    let rental;
    let movie;
   
    /**INSTEAD OF REPEATING OURSELVES, YOU COULD EXTRACT THIS PIECE
     * THAT WOULD BE REPEATED ACROSS TESTS, 
     * INITIALLY TAKE THE HAPPY FLOW, THEN MODIFY IN TEST
     */
    const executeRequest = () => {
        return request(server)
        .post('/api/returns')
        .set('x-auth-token', token) //set headers
        .send({customerId, movieId}); //in ES6, if the key and value is same, you could pass it like this.. instead of customerId : customerId

    }
    beforeEach(async () => { 
        server = require('../../../index');

        token = new User().generateToken();        
        customerId = mongoose.Types.ObjectId();
        movieId = mongoose.Types.ObjectId();


        movie = new Movie({
            _id: movieId,
            title: 'Oz',
            dailyRentalRate : 5,
            numberInStock : 7,
            genre : {
                name: 'my genre'
            }
        });

        await movie.save();
        rental = new Rental({
            customer : {
                _id : customerId,
                name: 'Seun',
                phone: '08084596644'
            },
            movie:{
                _id: movieId,
                title: 'Oz',
                dailyRentalRate : 5
            }
        });

        await rental.save();
    });
   
    afterEach(async() => {
        await server.close();
        await Rental.remove({});
        await Movie.remove({});
    });


    describe('POST /',  () => {
        it('should return 401, if user not logged in', async () => {
            token = '';

            const res = await executeRequest();

            expect(res.status).toBe(401);
        });
        it('should return 400, if customerId is not provided', async () => {
            
            customerId = '';

            const res = await executeRequest();

            expect(res.status).toBe(400);
        });
        it('should return 400, if movieId is not provided', async () => {
            movieId = '';

            const res = await executeRequest();

            expect(res.status).toBe(400);
        });
        it('should return 404, if no rental found for this customer/movie', async () => {
           
            await Rental.remove({});

            const res = await executeRequest();

            expect(res.status).toBe(404);
        });
        it('should return 400, if rental already processed', async () => {
            rental.dateReturned = new Date();
            await rental.save();

            const res = await executeRequest();

            expect(res.status).toBe(400);
        });
        it('should return 200 if its valid', async () => {
            

            const res = await executeRequest();

            expect(res.status).toBe(200);  
        });
        it('should set returnDate  if its valid', async () => {
            
            const res = await executeRequest();

            const rentalInDb = await Rental.findById(rental._id);

            expect(rentalInDb.dateReturned).not.toBeNull();  
        });
        it('should return accurate fee', async () => {
            
            //moment is a npm package for working with dates
            rental.dateRented = moment().add(-10,'days').toDate();           
            await rental.save();

            const res = await executeRequest();

            const rentalInDb = await Rental.findById(rental._id);


            expect(rentalInDb.price).toBe(50);  
        });

        it('should increase count of movie', async () => {
            
            const res = await executeRequest();

            const movieInDb = await Movie.findById(rental.movie._id);


            expect(movieInDb.numberInStock).toBe(movie.numberInStock + 1);  
        });

        it('should return rental is input is valid', async () => {
            
            const res = await executeRequest();


            expect(res.body).toHaveProperty('dateRented');  
            expect(res.body).toHaveProperty('dateReturned');  
            expect(res.body).toHaveProperty('price');  
            expect(res.body).toHaveProperty('customer');  
            expect(res.body).toHaveProperty('movie');  
        });
    });
});