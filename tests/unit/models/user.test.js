const {User} = require('../../../models/users');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const config = require('config');

describe('user.generateToken', () => {
    it('should return a valid jwt', () =>{
        const payload = { _id : mongoose.Types.ObjectId().toHexString(), isAdmin: true};
        const user = new User(payload);

        const token = user.generateToken();

        const decoded = jwt.verify(token, config.get('jwtPrivateKey'));

        expect(decoded).toMatchObject(payload);
    });
})