const config = require('config');
const logger = require('./middleware/logger');

const express = require('express');
const app = express();

//putting this first because we should be able to log, of others fail
require('./startup/logging')();
require('./startup/routes')(app);
require('./startup/config')();
require('./startup/database')(config.get('connectionString'));
require('./startup/validation');
require('./startup/prod')(app);
//throw new Error('Kilode');
//Promise.reject(new Error('Oh crap, it failed, smh')).then(() => console.log('how far?'));


const server = app.listen(3500, () => { logger.info(`Listening on port 3500`)});
module.exports = server;