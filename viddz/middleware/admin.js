function isAdmin (req, res, next){

    if(!req.user.isAdmin) return res.status(403).send('Access Denied, your level no reach this side');

    next();
}

module.exports = isAdmin;