const jwt = require('jsonwebtoken');
const config = require('config');

function auth(req, res, next){
    //request, response, next middleware function in the pipeline   

    const token = req.header('x-auth-token');
    if(!token) return res.status(401).send('Access denied, no token');

    //this 
    try{
        //this would return the decoded payload
        //if token is invalid, an exception would be thrown
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'));
        //crate an object on the req and assign the decoded
        req.user = decoded;
        next();
    }
    catch(e){
        res.status(400).send('Bad request');
    }
}

module.exports = auth;