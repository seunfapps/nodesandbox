const winston = require('winston');
//require('winston-mongodb');

const logger = winston.createLogger({
    //log level, in order
    /**
     * error
     * warn
     * info
     * verbose
     * debug
     * silly
     */
    transports : [
        //winston can log to console, file and http, even MongoDB and Redis
        //this will log to both console and file, you can add multiple ttansports
        new winston.transports.Console(),
        new winston.transports.File({filename: 'logs.log'}),
        //configuring the winston - mongodb
        //IF YOU WANT TO SET THAT ONLY MESSAGES OF LEVEL 'ERROR' BE LOGGED TO THE DB, YOU SIMPLY SET IT IN THE OPTIONS
        //IF YOU SET IT TO INFO, IT WOULD ALSO SAVE FOR WARN AND ERROR (BECAUSE ITS BELOW ITS LEVEL)
        //new winston.transports.MongoDB({ db: 'mongodb://localhost/viddz-db', level : 'error'})
    ],
    exceptionHandlers:[
        new winston.transports.File({filename: 'exlogs.log'}),
    ]
});



module.exports = logger;