const Joi = require('joi');
const mongoose = require('mongoose');
const {customerSchema} = require('./customers');

const rentalSchema =  new mongoose.Schema({
    price : {
        type: Number,
        min: 0,
        // required: true
    },
    dateRented : {
        type: Date,
        default: Date.now
    },
    dateReturned:{
        type: Date
    },

    customer: {
        type: customerSchema,
        required: true
    },
    movie: {
        type: new mongoose.Schema({
            title : {
                type: String,
                required: true,
                minlength: 1,
                maxlength: 255
            },
            dailyRentalRate:{
                type: Number,
                required: true,
                min: 0
            }
        }),
        required: true
    }

});

//statics keyword says this is a static method
//can be called Rental.lookup(seun, mortdecai)
rentalSchema.statics.lookup =  function (customerId, movieId){
    return this.findOne({
        'customer._id' : customerId,
        'movie._id' : movieId
    });
}
const Rental =  mongoose.model('rental', rentalSchema);

function validate(rental){
    const schema = {
        customerId: Joi.objectId().required(),
        movieId: Joi.objectId().required()    
    };

    return Joi.validate(rental, schema);
}

module.exports.Rental = Rental;
module.exports.validate = validate;