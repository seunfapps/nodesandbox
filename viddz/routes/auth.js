const bcrpyt = require('bcryptjs');
const express = require('express');
const Joi = require('joi');
const {User} = require('../models/users');
const router = express.Router();


router.post('/', async(req, res) => {
    const {error} = validate(req.body);
    if(error)
        return res.status(400).send(error.details[0].message);
    
    const user = await User.findOne({email: req.body.email});
    if(!user)
        return res.status(400).send('Invalid email/password');


    const valid = await bcrpyt.compare(req.body.password, user.password);
    if(!valid)
        return res.status(400).send('Invalid email/password');

    /**
     * Using JSON Web Tokens
     * A JWT is like a passport or driver's license or something that is issued to someone.
     * In this scenario, after a successful login.
     * So, for subsequent requests, the jwt would be passed, no need to go to db to validate
     * YOU COULD STORE SOME INFO IN THE JWT PAYLOAD, SO IT CAN BE USED WITHOUT GOING TO DB
     * So, you may want to be displaying a users name all around the site, you may want to know the users' role etc
     * 
     * So a JSON web token consists of 3 parts:
     * the Header,: contains info about the hashing algorithm used, and the type
     * the Payload,: containns actual data you want to store
     * the Signature: 
     * 
     * 
     * the 3 parts are encrypted into one string, seperated by '.', using the algorithm specified in the header
     *
     */

    //so now, after successful login, we want to generate a token and return it back to the client
    //the first parameter is the payload
    //the second parameter is secret private key, ideally it should not be hard coded
    //const token = jwt.sign({_id: user._id, name: user.name}, 'myPrivateKey');
    //picking private key from environmental variable
  
    //const token = jwt.sign({_id: user._id, name: user.name}, config.get('jwtPrivateKey'));

    //created method in the user model (schema)
    const token = user.generateToken();

    /**
     * Here I actuallty learnt how to set environment variables
     * in cmd : set mykey=myvalue, them boom, a variable is 'mykey' is created for you.
     * Now, the NODE_ENV is very important to, you can set it to whatever you like
     * Just make sure the name of your .json file in the config matches exactly.
     * 
     * The 'custom-environment-variables.json' file allows you to substitute the value in json for a environment variable
     * a way of securing your app, sensitive data should not be stored in codebase.
     */

    res.send(token);
});


function validate (auth){
    const schema = {
        email: Joi.string().required().min(3).email(),
        password: Joi.string().required().min(6)
    }

    return Joi.validate(auth, schema);
}

module.exports = router;