const express = require('express');
const {Movie, validate} = require('../models/movies');
const {Genre} = require('../models/genres');
const router = express.Router();

router.get('/', async(req, res) => {
    const movies = await Movie.find();
    res.send(movies);

});

router.get('/:id', async(req, res) => {
    const movie = await Movie.findById(req.params.id);
    if(!movie) res.status(404).send(`The movie with id ${req.params.id} was not found.` );
    res.send(movie);
});

router.post('/', async (req, res) => {
    if(!validate(req.body))
        return res.status(400).body('Invalid request');

    const genre = await Genre.findById(req.body.genreId);
   
    if(!genre)
        return res.status(400).send('Invalid genre');

    let movie = new Movie({
        title: req.body.title,
        genre: {
            _id: genre._id,
            name: genre.name
        },
        numberInStock: req.body.numberInStock,
        dailyRentalRate: req.body.dailyRentalRate
    });

    movie = await movie.save().catch(err => {
        console.log ('Oops, an error occured => ', err.errors);
        return res.status(500).send('An error occured while saving');
    });
    
    res.send(movie);
});

router.put('/', async (req, res) =>{
    if(!validate(req.body))
        return res.status(400).send('Invalid request');

    const genre = await Movie.findById(req.body.genreId);
    if(!genre)
        return res.status(400).send('Invalid genre');

    const movie = await Movie.update({_id: req.body._id}, {
        $set: {
            title: req.body.title,
            genre: {
                name: genre.name,
                _id: genre._id
            }
        }
    });

    res.send(movie);
});

router.delete('/:id', async (req, res) => {
    const movie = await Movie.findByIdAndRemove(req.params.id);
    res.send(movie);
});

module.exports = router;
