const config = require('config');

module.exports = function(){
    console.log('jwt key ', config.get('jwtPrivateKey'));

    if(!config.get('jwtPrivateKey')){
        throw new Error('FATAL, unable to find private key');
    
    }   
}

