const mongoose = require('mongoose');
const logger = require('../middleware/logger');
module.exports = function(connectionString){
    //connect to mongoose
    mongoose.connect(connectionString, {useNewUrlParser: true})
    .then(() => logger.info(`Successfully connected to ${connectionString}`))
}