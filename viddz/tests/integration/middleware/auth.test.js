const request = require('supertest');
const {User} = require('../../../models/users');
const {Genre} = require('../../../models/genres');
describe('auth middleware', () => {
    
    beforeEach(() => server = require('../../../index'));
  
    afterEach(async() => {
        await server.close();
        await Genre.remove({});
    });
    it('should return 401 if no token is providedd', async () => {
        const res = await request(server) 
            .post('/api/genres')            
            .send({name: 'Yooooo'});

        expect(res.status).toBe(401);

    });

    it('should return 400 if no token is invalid', async () => {
        const res = await request(server) 
            .post('/api/genres')   
            .set('x-auth-token', 'e')         
            .send({name: 'Yooooo'});

        expect(res.status).toBe(400);

    });

    it('should return 200 if  token is valid', async () => {
        const token = new User().generateToken();
        const res = await request(server) 
            .post('/api/genres')   
            .set('x-auth-token', token )         
            .send({name: 'Yooooo'});

        expect(res.status).toBe(200);

    })
})