const request = require('supertest');
const {Genre} =  require('../../../models/genres');
const {User} =  require('../../../models/users');
let server;
describe('/api/genres', () => {
    //runs before each test is run
    beforeEach(() => server = require('../../../index'));
    //runs after each test is run
    afterEach(async() => {
        await server.close();
        await Genre.remove({});
    });

    describe('GET', () => {
        it('should return all genres', async () => {

            await Genre.collection.insertMany([
                {name: 'genre1'},
                {name: 'genre2'},
                {name: 'genre3'},
            ]);

            const response = await request(server).get('/api/genres');

            expect(response.status).toBe(200);

            expect(response.body.length).toBe(3);

            //expect(response.body.some(x => x.name === 'genre1')).toBeTruthy;
        });
    });

    describe('GET /:id', () => {
        it('should return a genre object for valid id ', async () => {

            const genre = new Genre({name: 'My Genre'});
            await genre.save();

            const res = await request(server).get('/api/genres/'+ genre._id);

            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name','My Genre');
        });

        it('should return 404 if invalid id is passed ', async () => {

            const res = await request(server).get('/api/genres/242323');

            expect(res.status).toBe(404);
            
        });
    });

    describe('POST /',  () => {
        it('should return 401, if user not logged in', async () => {
            const res = await request(server)
                .post('/api/genres')
                .send({name : 'You Genre'});

            expect(res.status).toBe(401);
        });
        it('should return 400, if genre is less than 3 characters', async () => {
            const token = new User().generateToken();
            
            const res = await request(server)
                .post('/api/genres')
                .set('x-auth-token', token) //set headers
                .send({name : 'Ge'});

            expect(res.status).toBe(400);
        });
        it('should save the genre if its valid', async () => {
            const token = new User().generateToken();
            
            const res = await request(server)
                .post('/api/genres')
                .set('x-auth-token', token) //set headers
                .send({name : 'Seun Genre'});

            const genre = await Genre.find({name : 'Seun Genre'});

            expect(genre).not.toBeNull();
        });
        it('should return the genre if its valid', async () => {
            const token = new User().generateToken();
            
            const res = await request(server)
                .post('/api/genres')
                .set('x-auth-token', token) //set headers
                .send({name : 'Seun Genre'});

            
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', 'Seun Genre');
        })
    });
});